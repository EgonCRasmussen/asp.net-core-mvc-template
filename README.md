# README #

Dette er en ASP.NET Core MVC template, der indeholder 3 branches:

* master: HomeController med Index, About og Contact action method
* menu
* identity

### What is this repository for? ###

* Version 1.0.0-preview2-003131

### How do I get set up? ###

Skriv i en CMD: `git clone https://EgonCRasmussen@bitbucket.org/EgonCRasmussen/asp.net-core-mvc-template.git dit-eget-projekt-navn`

Åbn projektet i VS og checkout den ønskede branch

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact